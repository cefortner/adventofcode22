def isSeen(x, y, direction):
  if (direction == 0):
    while (x > 0):
      check = allTrees[x - 1][y]
      if (check < allTrees[x][y]):
        x = x - 1
        continue
      else:
        return False
    return True
  elif (direction == 1):
    while (x < inputLen-1):
      check = allTrees[x + 1][y]
      if (check < allTrees[x][y]):
        x = x + 1
        continue
      else:
        return False
    return True
  elif (direction == 2):
    while (y > 0):
      check = allTrees[x][y - 1]
      if (check < allTrees[x][y]):
        y = y - 1
        continue
      else:
        return False
    return True
  elif (direction == 3):
    while (y < 98):
      check = allTrees[x][y + 1]
      if (check < allTrees[x][y]):
        y = y + 1
        continue
      else:
        return False
    return True
  else:
    print("something went wrong")
    return False




f = open("input8.txt", "r")
input = f.readline()
input = input.rstrip()
input = [*input]
inputLen = len(input)
print("inputLen", inputLen)
allTrees = [[0]] * 99

i = 0
allTrees[0] = input
while (1):
  i += 1
  input = f.readline()
  if (input == ""):
    break
  input = input.rstrip()
  input = [*input]
  allTrees[i] = input

treesSeen = 0

i = 0
for x in range(0, 99):
  for y in range(0, 99):
    if(x == 0 or y==0):
      treesSeen +=1
      continue
    elif(x==inputLen or y==inputLen):
      treesSeen +=1
      continue
      
    i += 1
    for z in range(0, 4):
      if (isSeen(x, y, z) == True):
        treesSeen += 1
        break
      else:
        continue

print(i)
print(treesSeen)
# print("x=0",allTrees[0])
# print("y=0",allTrees[1][0])

#inputLen OR inputLen-1?
