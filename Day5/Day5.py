f = open("input5.txt", "r")
curInput = f.readline()

stack = [['Z', 'J', 'G'], ['Q', 'L', 'R', 'P', 'W', 'F', 'V', 'C'],
         ['F', 'P', 'M', 'C', 'L', 'G', 'R'],
         ['L', 'F', 'B', 'W', 'P', 'H', 'M'], ['G', 'C', 'F', 'S', 'V', 'Q'],
         ['W', 'H', 'J', 'Z', 'M', 'Q', 'T', 'L'], ['H', 'F', 'S', 'B', 'V'],
         ['F', 'J', 'Z', 'S'], ['M', 'C', 'D', 'P', 'F', 'H', 'B', 'T']]

#part1
# while(curInput != ""):
#   input = curInput.split(" ")
#   number = int(input[1])
#   moveFrom = int(input[3]) - 1
#   moveTo = int(input[5]) - 1

#   for i in range(0,number):
#     item = stack[moveFrom].pop()
#     stack[moveTo].append(item)

#   curInput = f.readline()

# for i in range(0,len(stack)):
#   print(stack[i][-1])

#part2
while (curInput != ""):
  input = curInput.split(" ")
  number = int(input[1])
  moveFrom = int(input[3]) - 1
  moveTo = int(input[5]) - 1

  items = []
  for i in range(0, number):
    item = stack[moveFrom].pop()
    items.insert(0, item)
  for i in range(0, len(items)):
    stack[moveTo].append(items[i])

  # for i in range(0,len(stack)):
  #   print(stack[i])
  # print("\n")
  curInput = f.readline()

for i in range(0, len(stack)):
  print(stack[i][-1])
