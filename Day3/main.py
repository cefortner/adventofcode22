f = open("input.txt")
dict = {
  'a': 1,
  'c': 3,
  'b': 2,
  'e': 5,
  'd': 4,
  'g': 7,
  'f': 6,
  'i': 9,
  'h': 8,
  'k': 11,
  'j': 10,
  'm': 13,
  'l': 12,
  'o': 15,
  'n': 14,
  'q': 17,
  'p': 16,
  's': 19,
  'r': 18,
  'u': 21,
  't': 20,
  'w': 23,
  'v': 22,
  'y': 25,
  'x': 24,
  'z': 26,
  'A': 27,
  'B': 28,
  'C': 29,
  'D': 30,
  'E': 31,
  'F': 32,
  'G': 33,
  'H': 34,
  'I': 35,
  'J': 36,
  'K': 37,
  'L': 38,
  'M': 39,
  'N': 40,
  'O': 41,
  'P': 42,
  'Q': 43,
  'R': 44,
  'S': 45,
  'T': 46,
  'U': 47,
  'V': 48,
  'W': 49,
  'X': 50,
  'Y': 51,
  'Z': 52
}

first, second, third = [], [], []
count = 0
total = 0
while (1):
  first = f.readline()
  if first == "":
    break
  second = f.readline()
  third = f.readline()

  firstItems = [*first]
  secondItems = [*second]
  thirdItems = [*third]
  common = []
  final = None
  for item in firstItems:
    if item in secondItems:
      if item in common:
        continue
      else:
        common.append(item)
  for item in common:
    if item in thirdItems:
      final = item
      total += dict[final]
      count = count + 1
      break
print(count)
print(total)
    # while (1):
#   curInput = f.readline()
#   if curInput == "":
#     break
#   start = 0
#   end = len(curInput)
#   half = end // 2
#   first = curInput[0:half]
#   second = curInput[half:-1]
  
#   firstItems = [*first]
#   secondItems = [*second]
#   done = 0
#   #print(firstItems, secondItems)
#   for item in firstItems:
#     if done == 1:
#       done = 0
#       break
#     for secondItem in secondItems:
#       if item == secondItem:
#         count = count + 1
#         total += dict[item]
#         done =1
#         break

# print(total)
# print(count)
