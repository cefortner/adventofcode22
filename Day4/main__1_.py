f = open("input.txt", "r")

enclosed = 0
intersection = 0
total = 0
while (1):
  curInput = f.readline()
  if curInput == "":
    break
  total += 1
  curInput = curInput.split(",")
  left = curInput[0]
  right = curInput[1]
  left = left.split("-")
  right = right.split("-")

  leftSet = set(range(int(left[0]), int(left[1]) + 1))
  rightSet = set(range(int(right[0]), int(right[1]) + 1))

  if leftSet.issubset(rightSet) | rightSet.issubset(leftSet):
    enclosed += 1
  if len(leftSet.intersection(rightSet)) > 0:
    intersection += 1

print("enclosed: " + str(enclosed))
print("intersection: " + str(intersection))
print("total counted: " + str(total))
