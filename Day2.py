f = open("input.txt", "r")

#X means lose
#y means tie
#z means win
total = 0
map = {"X": 1, "Y": 2, "Z": 3}
tie = {"A": "X", "B": "Y", "C": "Z"}
win = {"A": "Y", "B": "Z", "C": "X"}
lose = {"A": "Z", "B": "X", "C": "Y"}

while (1):
  curInput = f.readline()
  if str(curInput) == "":
    break
  curInput = curInput.split()

  if curInput[1] == "X":
    #print(curInput[0] + " and " + curInput[1])
    myplay = lose[curInput[0]]
    #print(myplay)
    total += map[myplay]
  elif curInput[1] == "Y":
    myplay = tie[curInput[0]]
    total += map[myplay]
    total += 3
  elif curInput[1] == "Z":
    myplay = win[curInput[0]]
    total += map[myplay]
    total += 6

print(total)
